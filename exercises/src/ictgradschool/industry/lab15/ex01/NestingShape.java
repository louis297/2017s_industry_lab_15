package ictgradschool.industry.lab15.ex01;

import java.util.ArrayList;
import java.util.List;
import java.awt.Graphics;

public class NestingShape extends Shape{
    private List<Shape> children = new ArrayList<>();

    NestingShape(){
        super();
    }

    public NestingShape(int x, int y){
        super(x,y);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY){
        super(x,y,deltaX,deltaY);
    }

    public NestingShape(int x, int y, int deltaX, int deltaY, int width, int height){
        super(x,y,deltaX,deltaY,width,height);
    }

    public void move(int width, int height){
        super.move(width, height);
        for (Shape child : children) {
            child.move(this.fWidth,this.fHeight);
        }
    }

    public void paint(Painter painter){
        // 1. paint self
        painter.drawRect(fX,fY,fWidth,fHeight);


        // 2. paint all children
        painter.translate(fX,fY);
        for (Shape child : children) {
            child.paint(painter);

        }
        painter.translate(-fX,-fY);
    }

    public void add(Shape child) throws IllegalArgumentException{
        if(child.parent() != null)
            throw new IllegalArgumentException();
        else if(checkBounds(child)) {
            children.add(child);
            child.setParent(this);
        } else {
            throw new IllegalArgumentException();
        }

    }

    public void remove(Shape child){
        children.remove(child);
        child.setParent(null);
    }

    public Shape shapeAt(int index) throws IndexOutOfBoundsException{
        return children.get(index);
    }

    public int shapeCount(){
        return children.size();
    }

    public int indexOf(Shape child){
        return children.indexOf(child);
    }

    public boolean contains(Shape child){
        return children.contains(child);
    }

    private boolean checkBounds(Shape child){
        if(child.fX < 0) return false;
        if(child.fY < 0) return false;
        if(child.fX + child.getWidth() > this.getWidth()) return false;
        if(child.fY + child.getHeight() > this.getHeight()) return false;

        return true;
    }

    public List<Shape> getChildren() {
        return children;
    }
}

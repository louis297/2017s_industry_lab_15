package ictgradschool.industry.lab15.ex02;



import ictgradschool.industry.lab15.ex01.NestingShape;
import ictgradschool.industry.lab15.ex01.Shape;

import javax.swing.event.TreeModelListener;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import java.util.Iterator;

public class TreeModelAdapter implements TreeModel {
    private NestingShape nestingShape;

    public TreeModelAdapter(NestingShape root) {
        nestingShape = root;
    }

    public Object getRoot() {
        return nestingShape;
    }

    public int getChildCount(Object parent) {
        int result = 0;
        Shape shape = (Shape) parent;

        if (shape instanceof NestingShape) {
            NestingShape ns = (NestingShape) shape;
            result = ns.getChildren().size();
        }
        return result;
    }

    @Override
    public boolean isLeaf(Object node) {
        return (!(node instanceof NestingShape));
    }

    @Override
    public void valueForPathChanged(TreePath path, Object newValue) {

    }

    public Object getChild(Object parent, int index) {
        Object result = null;

        if (parent instanceof NestingShape) {
            NestingShape ns = (NestingShape) parent;
            result = ns.shapeAt(index);
        }
        return result;
    }

    public int getIndexOfChild(Object parent, Object child) {
        int indexOfChild = -1;

        if (parent instanceof NestingShape) {
            NestingShape ns = (NestingShape) parent;
            Iterator<Shape> i = ns.getChildren().iterator();
            boolean found = false;

            int index = 0;
            while (!found && i.hasNext()) {
                Shape current = i.next();
                if (child == current) {
                    found = true;
                    indexOfChild = index;
                } else {
                    index++;
                }
            }
        }
        return indexOfChild;
    }

    @Override
    public void addTreeModelListener(TreeModelListener l) {

    }

    @Override
    public void removeTreeModelListener(TreeModelListener l) {

    }

}
